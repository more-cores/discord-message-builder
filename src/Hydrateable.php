<?php

namespace DiscordMessageBuilder;

interface Hydrateable
{
    public function hydrate(array $array): self;
}
